package facci.kleber.delgado.jugar;


import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import facci.kleber.delgado.jugar.menuinicio.Abecedario;
import facci.kleber.delgado.jugar.menuinicio.Mate;
import facci.kleber.delgado.jugar.menuinicio.Vocales;


public class Main2Activity extends AppCompatActivity {
    Button btn1,btn2,btn3;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);
    btn1=(Button)findViewById(R.id.btnumero);
    btn2=(Button)findViewById(R.id.btnabc);
    btn3=(Button)findViewById(R.id.btnvocal);

    btn1.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Intent intent = new Intent(Main2Activity.this,Mate.class);
            startActivity(intent);
        }
    });

    btn2.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Intent intent=new Intent (Main2Activity.this,Abecedario.class);
            startActivity(intent);
        }
    });

    btn3.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Intent intent=new Intent (Main2Activity.this,Vocales.class);
            startActivity(intent);
        }
    });

    }
}
