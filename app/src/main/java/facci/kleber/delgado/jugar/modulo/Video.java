package facci.kleber.delgado.jugar.modulo;

public class Video {
    private String Primer;
    private String Segundo;
    private String Tercero;
    public Video(){

    }

    public String getPrimer() {
        return Primer;
    }

    public void setPrimer(String primer) {
        Primer = primer;
    }

    public String getSegundo() {
        return Segundo;
    }

    public void setSegundo(String segundo) {
        Segundo = segundo;
    }

    public String getTercero() {
        return Tercero;
    }

    public void setTercero(String tercero) {
        Tercero = tercero;
    }
}